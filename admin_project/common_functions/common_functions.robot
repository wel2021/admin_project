*** Settings ***
Library    Process
Library    OperatingSystem
Library    RequestsLibrary

*** Variables ***
${URL}                  https://www.google.com
${CHROMEDRIVER_PATH}    /webdriver

*** Keywords ***
Launch WebPage
    [Documentation]            Launching the page
    [Arguments]                ${url}                                                                                                                                                                                                                                                                                                            #${browser}
    # Open Chrome Browser
    # Go To                  ${url}
    # Get The Request After Launch The WebPage
    # Headless Chrome - Create Webdriver    ${url}
    # Headless Chrome - Open Browser    ${url}
    Open Browser               ${url}                                                                                                                                                                                                                                                                                                            chrome
    ...                        options=add_argument("--headless"); add_argument("--window-size=1920x1080"); add_argument("--disable-dev-shm-usage"); add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors"); add_argument("--no-sandbox"); add_argument("--disable-extensions"); add_argument("--disable_gpu")
    Maximize Browser Window
Headless Chrome - Create Webdriver
    [Arguments]            ${url}
    ${chrome_options} =    Evaluate             sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method            ${chrome_options}    add_argument                                         headless
    Call Method            ${chrome_options}    add_argument                                         disable-gpu
    ${options}=            Call Method          ${chrome_options}                                    to_capabilities            

    Create Webdriver    Remote    command_executor=http://127.0.0.1:56006/wd/hub    desired_capabilities=${options}

    Go to    ${url}

    Maximize Browser Window
    Capture Page Screenshot

Headless Chrome - Open Browser
    [Arguments]            ${url}
    ${chrome_options} =    Evaluate             sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method            ${chrome_options}    add_argument                                         headless
    Call Method            ${chrome_options}    add_argument                                         disable-dev-shm-usage
    Call Method            ${chrome_options}    add_argument                                         disable-gpu
    ${options}=            Call Method          ${chrome_options}                                    to_capabilities            

    Open Browser    ${url}    browser=headlesschrome    desired_capabilities=${options}    #remote_url=http://localhost:4444/wd/hub

    Maximize Browser Window
    Capture Page Screenshot

Open Chrome Browser
    [Documentation]      Open the Chrome Browser with maximize view, no infobars notification and no pop-ups.
    ${chrome_options}    Evaluate                                                                                sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    # ${chrome_capabilities}    Call Method                                                                             ${chrome_options}                                    to_capabilities
    # Set To Dictionary         ${chrome_capabilities["goog:chrome_options"]}                                           debuggerAddress                                      127.0.0.1:9222
    ${prefs}             Create Dictionary                                                                       credentials_enable_service=${false}

    #Option 1
    # Call Method    ${chrome_options}    add_experimental_option    prefs                       ${prefs}
    # Call Method    ${chrome_options}    add_argument               --start-maximized
    # Call Method    ${chrome_options}    add_argument               --disable-popup-blocking

    # # Option 2
    Call Method         ${chrome_options}    add_experimental_option             prefs                       ${prefs}
    Call Method         ${chrome_options}    add_argument                        disable-web-security
    Call Method         ${chrome_options}    add_argument                        --start-maximized
    Call Method         ${chrome_options}    add_argument                        --disable-dev-shm-usage
    Call Method         ${chrome_options}    add_argument                        --no-sandbox
    Call Method         ${chrome_options}    add_argument                        --disable-popup-blocking
    Create WebDriver    Chrome               chrome_options=${chrome_options}

Verify Launched Page
    [Documentation]                     Verify if page is launched successfully
    Sleep                               3
    Wait Until Page Contains Element    ${LOGIN_DISPLAY}                           10
    Element Text Should Be              ${LOGIN_DISPLAY}                           Email*    #Password
    Log To Console                      Launched Page Verified


Get The Request After Launch The WebPage
    Create Session     Get Details                               ${URL}
    ${response}        GET On Session                            Get Details    /login
    ${integer}         Convert To Integer                        200
    Should Be Equal    ${response.status_code}                   ${integer} 
    Log To Console     Status code is ${response.status_code}

Expand Menu Using Hamburger
    [Documentation]                  This is the HAMBURGER BUTTON
    Sleep                            2
    Wait Until Element Is Enabled    ${HAMBURGER_BTN}
    ${expand}                        Get Element Count               ${HAMBURGER_EXPAND}
    Run Keyword If                   "${expand}"=="1"                Wait Until Page Contains Element    ${HAMBURGER_EXPAND}    10

    Run Keyword If    "${expand}"=="0"                    Run Keywords           Click Element    ${HAMBURGER_BTN}    AND    
    ...               Wait Until Page Contains Element    ${HAMBURGER_EXPAND}    10
    Log To Console    Hamburger Menu Is Expanded

Select Menu
    [Documentation]                  Menu at the side portion
    [Arguments]                      ${menu}
    Sleep                            1
    Wait Until Element Is Enabled    ${MENU_LOCATORS}\[contains(text(),"${menu}")]    10
    Set Focus To Element             ${MENU_LOCATORS}\[contains(text(),"${menu}")]
    Click Element                    ${MENU_LOCATORS}\[contains(text(),"${menu}")]
    Log To Console                   Menu Selected

Click Button Create
    [Documentation]                     COMMON FUNCTION : Button To Create A Data
    Sleep                               1
    Scroll Element Into View            ${CREATE_BTN}
    Wait Until Page Contains Element    ${CREATE_BTN}
    Click Element                       ${CREATE_BTN}

Click Yes Button
    [Documentation]    COMMON FUNCTION : Yes Button
    Click Element      ${CONFIRM_YES}

Click Button Reset
    Sleep                               3
    Scroll Element Into View            ${RESET_BTN}
    Wait Until Page Contains Element    ${RESET_BTN}            10
    Sleep                               3
    Click Element                       ${RESET_BTN}
    Log To Console                      Button Reset Clicked

Click Delete Button From PopUp Dialog
    [Documentation]                     COMMON FUNCTION : Delete Button From PopUp Dialog
    [Arguments]                         ${user}
    Wait Until Page Contains Element    ${CONFIRM_POP_DELETE}                                10
    Click Element                       ${DELETE_BTN}
    Sleep                               1
    FOR                                 ${item}                                              IN RANGE    1    10
    Log                                 ${item} : Searching...
    Sleep                               1
    # Wait Until Element Is Visible       ${PROMPT_TOAST}\[normalize-space()="Delete Agency Successfully"]    10
    # Wait Until Page Contains            Delete Agency Successfully                           10
    # Sleep                               2
    Log To Console                      PopUp Dialog Delete Button
    END

Check Dialog Window Create User
    [Documentation]                     COMMON FUNCTION : When Dialog Windows screen like USER MANAGEMENT, ROLE MANAGEMENT
    [Arguments]                         ${dialog_header}
    Wait Until Page Contains Element    ${dialog_header}                                                                      #${CREATE_USER_DIALOG}

Check Dialog Window If No Exist
    [Documentation]                             COMMON FUNCTION : When Dialog Windows screen like USER MANAGEMENT, ROLE MANAGEMENT
    [Arguments]                                 ${dialog_header}
    Wait Until Page Does Not Contain Element    ${dialog_header}
    Log To Console                              Check Dialog Window If No Exist                                                       #${CREATE_USER_DIALOG}

Check If Header Is Present
    [Documentation]                COMMON FUNCTION : Name Of The Header From The Page
    [Arguments]                    ${locator_header}                                     ${header}
    Wait Until Element Contains    ${locator_header}                                     ${header}
    Log To Console                 Check If Header Is Present

Download The Latest Webdriver
    [Documentation]    COMMON FUNCTION : chrome and firefox
    Start Process      webdrivermanager chrome firefox --linkpath ${OUTPUT DIR}${/}webs    shell=True
    Sleep              10
    ${path}            Normalize Path                                                      C:\\webdriver
    ${files}           List Files In Directory                                             ${OUTPUT DIR}${/}webs

    FOR               ${file}                             IN         @{files}
    Move File         ${OUTPUT DIR}${/}webs${/}${file}    ${path}
    Log To Console    Download The Latest Webdriver
    END

Showing Pagination Display
    [Arguments]           ${reference_column}
    [Documentation]       COMMON FUNCTION : Pagination Display
    ${count}              Get Element Count                       ${reference_column}
    ${list_pagination}    Create List                             ${count}
    ${text}               Get Text                                //span[@class="p-paginator-current"]    #[normalize-space()="Showing 1 to 10 of 11 products"]
    Log                   ${text}
    ${split}              Split String From Right                 ${text}                                 ${SPACE}
    Log                   ${split}
    Log                   ${split}[0]
    Log                   ${split}[1]
    Log                   ${split}[2]
    Log                   ${split}[3]
    Log                   ${split}[4]
    Log                   ${split}[5]
    ${set_list}           Set Variable                            ${split}[5]
    ${var_1}              Set Variable                            ${set_list}
    ${var_2}              Set Variable                            1
    ${var_3}              Set Variable                            ${var_1} + ${var_2}
    Log                   ${var_3}
    Set Suite Variable    ${var_3}
    Set Suite Variable    ${list_pagination}

Scroll Down The Page
    [Documentation]       COMMON FUNCTION : Scrolling to down the page to view the element
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)
    Log To Console        Scrolling Down The Page

Scroll Up The Page
    [Documentation]       COMMON FUNCTION : Scrolling to up the page to view the element
    Execute JavaScript    window.scrollTo(0, -document.body.scrollHeight)
    Log To Console        Scrolling Up The Page