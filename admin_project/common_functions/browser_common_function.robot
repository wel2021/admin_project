*** Keywords ***
Launch Web Application
    [Arguments]        ${url}                                   ${browser}=${None}
    New Browser        headless=True                            #slowMo=0:00:01                                                                             #devtools=True
    ${har}             Create Dictionary                        path=${CURDIR}${/}har.file                                                                  omitContent=True
    ${size}            Create Dictionary                        dir                                                                                         ${OUTPUT_DIR}           width               1280                   height                                    1024
    ${record_video}    Create Dictionary                        dir                                                                                         ${OUTPUT_DIR}
    New Context        viewport={'width':1500, 'height':800}    colorScheme=dark                                                                            acceptDownloads=True    recordHar=${har}    recordVideo=${size}    #this is video recording and capturing
    # Open Browser    ${url}    chromium    pause_on_failure=True
    New Page           ${url}                                   # use this when you want to capture a video together with the New Context function above
    &{get}             Http                                     ${url}
    Should Be Equal    ${get.status}                            ${200}
    Get Title          ==                                       ${TITLE}

Input Valid UserName And Password
    [Arguments]                ${username_string}                                         ${password_string}
    ${old}                     Set Browser Timeout                                        30
    Fill Text                  ${USER_EMAIL}                                              ${username_string}
    Set Browser Timeout        ${old}
    Fill Secret                ${USER_PASSWORD}                                           $password_string                                           #$PASSWORD
    Click                      ${SUBMIT_BTN}
    Sleep                      3
    ${element}                 Get Element Count                                          ${PROMPT_TOAST}\[contains(text(), "${PROMPT_SUCCESS}")]
    IF                         "${element}" > "0"
    Wait For Elements State    ${PROMPT_TOAST}\[contains(text(), "${PROMPT_SUCCESS}")]    visible
    Wait For Elements State    ${SUBMIT_BTN}                                              hidden
    END

LogOut User
    Wait For Elements State    ${CURSOR_POINTER}      visible                    10
    Click                      ${CURSOR_POINTER}
    FOR                        ${index}               IN RANGE                   1                          11
    Log                        Looking at ${index}
    ${element}                 Get Element Count      ${LOGOUT_OPTION}
    Run Keyword If             ${element} > 1         Run Keywords               Wait For Elements State    ${LOGOUT_OPTION}    visible    10
    ...                        AND                    Hover                      ${LOGOUT_OPTION}
    ...                        AND                    Click                      ${LOGOUT_OPTION}           delay=900ms
    ...                        AND                    Wait For Elements State    ${LOGOUT_OPTION}           hidden              10
    ...                        AND                    Wait For Elements State    ${SUBMIT_BTN}              visible             10
    ...                        AND                    Exit For Loop
    END
    ${string}                  Convert To String      ${element}
    Should Be Equal            ${string}              1

Click Hamburger Button
    [Documentation]            COMMON FUNCTION
    Wait For Elements State    ${HAMBURGER_BTN}     visible
    Highlight Elements         ${HAMBURGER_BTN}     duration=01                width=4px
    ${element}                 Get Element Count    ${HAMBURGER_EXPAND}
    Run Keyword If             ${element} > 0       Wait For Elements State    ${HAMBURGER_EXPAND}    visible
    Run Keyword If             ${element} == 0      Run Keywords               Click                  ${HAMBURGER_BTN}
    ...                        AND                  Wait For Elements State    ${HAMBURGER_EXPAND}    visible

Click Create Button
    [Documentation]            COMMON FUNCTION
    Wait For Elements State    ${CREATE_BTN}      enabled
    Click                      ${CREATE_BTN}

Select Menu From SideBar
    [Arguments]                ${menu}
    [Documentation]            COMMON FUNCTION
    Wait For Elements State    ${MENU_LOCATORS}\[contains(text(),"${menu}")]    visible        10
    Highlight Elements         ${MENU_LOCATORS}\[contains(text(),"${menu}")]    duration=01    width=4px
    Sleep                      2
    Click                      ${MENU_LOCATORS}\[contains(text(),"${menu}")]

Check If Header Is Visible
    [Arguments]                ${header_name}
    [Documentation]            COMMON FUNCTION
    Wait For Elements State    ${USER_MGT_HEADER}\[normalize-space()="${header_name}"]    visible        10
    Highlight Elements         ${USER_MGT_HEADER}\[normalize-space()="${header_name}"]    duration=01    width=4px

Click Delete Button From PopUp Message
    [Arguments]                     ${user}
    [Documentation]                 COMMON FUNCTION : Delete Button From PopUp Dialog
    # Wait For Elements State    ${CONFIRM_POP_DELETE}    visible    10
    Click                           ${DELETE_BTN}
    Run Keyword And Ignore Error    Run Keywords                                         Wait For Elements State    ${PROMPT_TOAST}\[normalize-space()="Delete User ${user} Successfully"]    visible    10
    ...                             AND                                                  Wait For Elements State    ${PROMPT_TOAST}\[normalize-space()="Delete User ${user} Successfully"]    hidden     10

Showing Pagination Display
    [Arguments]           ${reference_column}
    [Documentation]       COMMON FUNCTION : Pagination Display
    ${count}              Get Element Count                       ${reference_column}
    ${list_pagination}    Create List                             ${count}
    ${text}               Get Text                                //span[@class="p-paginator-current"]    #[normalize-space()="Showing 1 to 10 of 11 products"]
    Log                   ${text}
    ${split}              Split String From Right                 ${text}                                 ${SPACE}
    Log                   ${split}
    Log                   ${split}[0]
    Log                   ${split}[1]
    Log                   ${split}[2]
    Log                   ${split}[3]
    Log                   ${split}[4]
    Log                   ${split}[5]
    ${set_list}           Set Variable                            ${split}[5]
    ${var_1}              Set Variable                            ${set_list}
    ${var_2}              Set Variable                            1
    ${var_3}              Set Variable                            ${var_1} + ${var_2}
    Log                   ${var_3}
    Set Suite Variable    ${var_3}
    Set Suite Variable    ${list_pagination}

Click Reset Button
    Wait For Elements State    ${RESET_BTN}    visible    10
    Click                      ${RESET_BTN}

Click Paginator Double Left
    Wait For Elements State    ${PAGINATE_DOUBLE_LEFT}    stable                     10                  # ${enable}          Get Element Count    ${PAGINATE_DOUBLE_LEFT}    # ${disable}                   Get Element Count    ${DISABLE_PAGE_DOUBLE_LEFT}    # Run Keyword If    "${enable}"=="1"               Run Keywords    Focus    ${PAGINATE_DOUBLE_LEFT}    # ...    # AND
    ...                        # Click                    ${PAGINATE_DOUBLE_LEFT}    # Run Keyword If    "${disable}"=="1"    Run Keywords         Focus                      ${DISABLE_PAGE_DOUBLE_LEFT}    # ...                # AND                          Click               ${DISABLE_PAGE_DOUBLE_LEFT}
    Click                      ${PAGINATE_DOUBLE_LEFT}

Count Available ToolTips
    [Documentation]            Count Number Of ToolTips based on the assigned role to the user
    Sleep                      3
    ${count_element}           Get Element Count                                                  ${TOOLTIP_SYMBOLS}
    ${count_list}              Create List                                                        ${count_element}
    FOR                        ${item}                                                            IN RANGE              @{count_list}
    ${count}                   Evaluate                                                           ${item} + 1
    Wait For Elements State    ${TOOLTIP_SYMBOL_2S}/tr[${count}]/td[4]/div/child::div[1]          visible               10
    Highlight Elements         ${TOOLTIP_SYMBOL_2S}/tr[${count}]/td[4]/div/child::div[1]          duration=200ms        style=solid      color=red 
    Log                        ${item} Element is found
    END
    Set Suite Variable         ${count_list}

Count Available Edit Buttons
    [Arguments]       ${exists}
    ${count}          Get Element Count                      ${EDIT_BTN_SYMBOLS}
    Log               Check Edit Buttons Count : ${count}
    ${count_edit}     Create List                            ${count}
    FOR               ${item}                                IN RANGE                                                      @{count_list}
    ${count}          Evaluate                               ${item} + 1
    Run Keyword If    "${exists}"=="VISIBLE"                 Run Keywords
    ...               Highlight Elements                     ${EDIT_BTN_SYMBOLS_2}/tr[${count}]/td[4]/div/child::div[2]    duration=200ms                                                style=solid    color=red 
    ...               AND                                    Wait For Elements State                                       ${EDIT_BTN_SYMBOLS_2}/tr[${count}]/td[4]/div/child::div[2]    enabled        10
    Run Keyword If    "${exists}"=="HIDDEN"                  Wait For Elements State                                       ${EDIT_BTN_SYMBOLS}/tr[${count}]/td[4]/div/child::div[2]      hidden         10
    END
    Log               ${count_edit}

Count Available Delete Buttons
    [Arguments]        ${exists}
    [Documentation]    USER MANAGEMENT
    ${count}           Get Element Count                        ${DELETE_BTN_SYMBOLS}
    Log                Check Delete Buttons Count : ${count}
    ${count_delete}    Create List                              ${count}
    FOR                ${item}                                  IN RANGE                                                      @{count_list}
    ${count}           Evaluate                                 ${item} + 1
    Run Keyword If     "${exists}"=="VISIBLE"                   Run Keywords                                                  
    ...                Highlight Elements                       ${DELETE_BTN_SYMBOLS}/tr[${count}]/td[4]/div/child::div[3]    duration=200ms                                                style=solid    color=red 
    ...                AND                                      Wait For Elements State                                       ${DELETE_BTN_SYMBOLS}/tr[${count}]/td[4]/div/child::div[3]    enabled        10
    Run Keyword If     "${exists}"=="HIDDEN"                    Wait For Elements State                                       ${DELETE_BTN_SYMBOLS}/tr[${count}]/td[4]/div/child::div[3]    hidden         10
    END
    Log                ${count_delete}
