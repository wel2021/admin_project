*** Settings ***
*** Settings ***
Documentation    This robot file is about USER MANAGEMENT. Basically it will 
...              CREATE, EDIT, and DELETE user
Library          SeleniumLibrary
Library          Collections
Resource         login_screen_simple_login.robot
Resource         keywords/role_management_keywords.robot
Resource         common_functions/common_functions.robot
Resource         ../common_libraries/screen_capture.robot

Suite Setup      Run Keywords  Launch WebPage  ${URL}   ${BROWSER}  AND  Start Capturing Video   0
Test Setup       Verify Launched Page
# Test Teardown    Logout Me
# Suite Teardown   Run Keywords  Stop Capturing Video  AND  Close Browser
Suite Teardown   Stop Capturing Video


*** Test Cases ***
TestOne_Create Simple User With Role
    [Tags]  TestOne
    Input Username And Password  ${ADMIN_CRED}  ${PASSWORD}
    Expand Menu Using Hamburger
    Select Menu  ${ROLE_MANAGEMENT}
    Check If Header Is Present  ${USER_MGT_HEADER}  ${ROLE_MANAGEMENT}
    Click Button Create
    Input Desired Role Name    Automation
    Verify Results In Table Role Management  Automation
    