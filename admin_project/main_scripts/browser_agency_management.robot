*** Settings ***
Documentation       AGENCY MANAGEMENT SCRIPT
Library             Browser                                                 enable_playwright_debug=False    #enable_presenter_mode=True    
Library             Collections
Library             String
Library             OperatingSystem
Library             FakerLibrary
Resource            ../common_functions/browser_common_function.robot
Resource            ../keywords/variables.robot
# Resource          ../common_libraries/screen_capture.robot 
Resource            ../keywords/browser_agency_management_keywords.robot
# Suite Setup       Start Capturing Video                                   0
# Suite Teardown    Run Keywords                                            Stop Capturing Video             AND                            Close Browser    ALL

Suite Teardown    Close Browser    ALL

*** Test Cases ***
Create Agency Account
    [Tags]                               TestOne
    Launch Web Application               ${URL}                  
    Input Valid UserName And Password    ${ADMIN_CRED}           ${PASSWORD}
    Click Hamburger Button
    Select Menu From SideBar             ${AGENCY_MANAGEMENT}
    Check If Header Is Visible           ${AGENCY_MANAGEMENT}
    List Of Agency Accounts              ONE                     ../${AGENCY_MANAGEMENT_CSV}
    LogOut User
    Close Browser


Delete Agency Account
    [Tags]                               TestFive
    Launch Web Application               ${URL}                  
    Input Valid UserName And Password    ${ADMIN_CRED}           ${PASSWORD}
    Click Hamburger Button
    Select Menu From SideBar             ${AGENCY_MANAGEMENT}
    Check If Header Is Visible           ${AGENCY_MANAGEMENT}
    List Of Agency Accounts              FIVE                    ../${AGENCY_MANAGEMENT_CSV}
    LogOut User
    Close Browser
