*** Settings ***
Documentation    Authentication Sign-In Page
Library          SeleniumLibrary
Library          WatchUI  outputs_folder=${OUTPUT DIR}  #ssim_basic=${SSIM}  format_image=${PNG}  tesseract_path=
Library          String
Resource         keywords.robot
Resource         variables.robot
Resource         login_screen_simple_login.robot
Suite Setup      Launch WebPage  ${URL}  ${BROWSER}
Test Setup       Verify Launched Page
Test Teardown    ${NONE}
Suite Teardown   Close Browser

*** Test Cases ***
# Open zrch
#     Sleep  5
#     Compare Screen    ${CURDIR}${/}zrchscreen.png

Enter Login Screen With Verify Error Message
    [Tags]  blankerror
    Verify Error Message From Blank Username And Password

# Verify Wrong Password
#     [Tags]  wrongpass
#     Input Text   ${USER_EMAIL}    ${ADMIN_CRED}
#     Input Password     ${USER_PASSWORD}  !!!!!@@222222
#     Click Element      ${SUBMIT_BTN}
#     Toast Prompt Summary   ${PROMPT_FAIL}

Enter Valid Email And Password
    [Tags]  validemail
    Input Username And Password
    Toast Prompt Summary  ${PROMPT_SUCCESS}
    # Logout Me
