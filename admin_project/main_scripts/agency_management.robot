*** Settings ***
Documentation    This robot file is about USER MANAGEMENT. Basically it will 
...              CREATE, EDIT, and DELETE user
Library          SeleniumLibrary
Library          Collections
Library          FakerLibrary
Library          String
Library          OperatingSystem
Resource         ../keywords/login_screen_simple_login.robot
Resource         ../keywords/agency_management_keywords.robot
Resource         ../common_functions/common_functions.robot
# Resource       ../../common_libraries/screen_capture.robot

Suite Setup    Run Keywords    Download The Latest Webdriver    AND    Launch WebPage    ${URL}    #AND    Start Capturing Video    0

Test Setup        Verify Launched Page
#Test Teardown    Logout Me
Suite Teardown    Close Browser


*** Test Cases ***
TestOne _Create Agency Users
    [Tags]                         TestOne
    Input Username And Password    ${ADMIN_CRED}           ${PASSWORD}
    Expand Menu Using Hamburger
    Select Menu                    ${AGENCY_MANAGEMENT}
    Check If Header Is Present     ${USER_MGT_HEADER}      ${AGENCY_MANAGEMENT}
    List Of Agency Accounts        ONE                     ../${AGENCY_MANAGEMENT_CSV}
    Logout Me


TestFive_Delete Created Agency
    [Tags]                         TestFive
    Input Username And Password    ${ADMIN_CRED}           ${PASSWORD}
    Expand Menu Using Hamburger
    Select Menu                    ${AGENCY_MANAGEMENT}
    Check If Header Is Present     ${USER_MGT_HEADER}      ${AGENCY_MANAGEMENT}
    List Of Agency Accounts        FIVE                    ../${AGENCY_MANAGEMENT_CSV}
    Logout Me