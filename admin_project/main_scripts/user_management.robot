*** Settings ***
Documentation    This robot file is about USER MANAGEMENT. Basically it will 
...              CREATE, EDIT, and DELETE user
Library          SeleniumLibrary
Library          Collections
Library          Dialogs
Resource         ../keywords/login_screen_simple_login.robot
Resource         ../keywords/user_management_keywords.robot
Resource         ../common_functions/common_functions.robot
# Resource       ../../common_libraries/screen_capture.robot

Suite Setup    Run Keywords    Download The Latest Webdriver    AND    Launch WebPage    ${URL}    #AND    Start Capturing Video    0

Test Setup          Verify Launched Page
#Test Teardown      Logout Me
# Suite Teardown    Run Keywords            Stop Capturing Video    AND    Close Browser
Suite Teardown      Close Browser



*** Test Cases ***
TestOne _Simple Create User Non Admin
    [Tags]                         TestOne
    Pause Execution                Docker
    Input Username And Password    ${ADMIN_CRED}         ${PASSWORD}
    Expand Menu Using Hamburger
    Select Menu                    ${USER_MANAGEMENT}
    Check If Header Is Present     ${USER_MGT_HEADER}    ${USER_MANAGEMENT}
    Send File Informations         TEST_ONE              ../${USER_MANAGEMENT_CSV}
    Logout Me

TestTwo_Perform Update Password After Creating New User Non-Admin
    [Tags]                    TestTwo
   # Input Username And Password  ${NAME_STRING}@mailinator.com  ${PASSWORD_STRING}
   # Input Update Password Fields  ${NEW_PASSWORD_STRING}
   # Input Username And Password  ${NAME_STRING}@mailinator.com  ${NEW_PASSWORD_STRING}
    Send File Informations    TEST_TWO    ${USER_MANAGEMENT_CSV}

TestThree_Perform Update Role From Non-Admin To Admin
    [Tags]                                     TestThree
    Input Username And Password                ${ADMIN_CRED}         ${PASSWORD}
    Expand Menu Using Hamburger
    Select Menu                                ${USER_MANAGEMENT}
    Check If Header Is Present                 ${USER_MGT_HEADER}    ${USER_MANAGEMENT}
   # Click Edit Button To Update User Profile  ${NAME_STRING}@mailinator.com   agency  admin
   # Input Username And Password  ${NAME_STRING}@mailinator.com  ${NEW_PASSWORD_STRING}
    Send File Informations                     TEST_THREE            ../${USER_MANAGEMENT_CSV}
    Select Menu                                ${ROLE_MANAGEMENT}
    Check If Header Is Present                 ${USER_MGT_HEADER}    ${ROLE_MANAGEMENT}
    Verify User Under Administrator Account
    Logout Me

TestFour_Perform Update User To Delete Role And Input Permissions
    [Tags]                         TestFour
    Input Username And Password    ${ADMIN_CRED}         ${PASSWORD}
    Expand Menu Using Hamburger
    Select Menu                    ${USER_MANAGEMENT}
    Check If Header Is Present     ${USER_MGT_HEADER}    ${USER_MANAGEMENT}

TestFive_Delete User
    [Tags]                         TestFive
    Input Username And Password    ${ADMIN_CRED}         ${PASSWORD}
    Expand Menu Using Hamburger
    Select Menu                    ${USER_MANAGEMENT}
    Check If Header Is Present     ${USER_MGT_HEADER}    ${USER_MANAGEMENT}
    Send File Informations         TEST_FIVE             ../${USER_MANAGEMENT_CSV}
    Logout Me

TestSix_Access Using Registered Non-Admin Account
    [Tags]                         TestSix
    Send File Informations         TEST_SIX              ../${USER_MANAGEMENT_CSV}
    Expand Menu Using Hamburger
    Select Menu                    ${USER_MANAGEMENT}
    Check If Header Is Present     ${USER_MGT_HEADER}    ${USER_MANAGEMENT}