*** Settings ***
Library          Browser                                               #enable_playwright_debug=True    #enable_presenter_mode=True
Library          Collections
Library          String
Library          OperatingSystem
Resource         ../common_functions/browser_common_function.robot
Resource         ../keywords/variables.robot
# Resource       ../common_libraries/screen_capture.robot
Resource         ../keywords/browser_user_management_keywords.robot
# Test Setup     Start Capturing Video                                 0
Test Teardown    Close Browser

*** Test Cases ***
TestOne _Simple Create User
    [Tags]                                 TestOne
    Launch Web Application                 ${URL}
    Input Valid UserName And Password      ${ADMIN_CRED}                                                  ${PASSWORD}
    Click Hamburger Button
    Select Menu From SideBar               ${USER_MANAGEMENT}
    Check If Header Is Present             ${USER_MGT_HEADER}\[normalize-space()="${USER_MANAGEMENT}"]
    Get File And Send File Informations    ONE                                                            ../${USER_MANAGEMENT_CSV}
    LogOut User
    Close Page

TestTwo_Perform Update Password
    [Tags]                                 TestTwo
    Launch Web Application                 ${URL}
    Get File And Send File Informations    TWO        ${USER_MANAGEMENT_CSV}
    Close Page

TestFive_Delete User
    [Tags]                                 TestFive
    Launch Web Application                 ${URL}
    Input Valid UserName And Password      ${ADMIN_CRED}                                                  ${PASSWORD}
    Click Hamburger Button
    Select Menu From SideBar               ${USER_MANAGEMENT}
    Check If Header Is Present             ${USER_MGT_HEADER}\[normalize-space()="${USER_MANAGEMENT}"]
    Get File And Send File Informations    FIVE                                                           ../${USER_MANAGEMENT_CSV}
    LogOut User
    Close Page

TestSix_Create User - Update Password - Delete User
    [Tags]                                 TestSix
    Launch Web Application                 ${URL}
    Input Valid UserName And Password      ${ADMIN_CRED}                                                  ${PASSWORD}
    Click Hamburger Button
    Select Menu From SideBar               ${USER_MANAGEMENT}
    Check If Header Is Present             ${USER_MGT_HEADER}\[normalize-space()="${USER_MANAGEMENT}"]
    Get File And Send File Informations    SIX                                                            ../${USER_MANAGEMENT_CSV}
    LogOut User
    Close Page
