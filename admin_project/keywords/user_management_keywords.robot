*** Settings ***
Library    String
Library    OperatingSystem


*** Keywords ***
Send File Informations
    [Arguments]    ${info}           ${csv}
    ${contents}    Get File          ${csv}
    @{read}        Create List       ${contents}
    @{lines}       Split To Lines    @{read}        1

    FOR      ${line}         IN         @{lines}
    ${um}    Split String    ${line}    ,

    ${user}    Set Variable    ${um}[0]

    ${domain}    Set Variable    ${um}[1]

    ${oldpass}    Set Variable    ${um}[2]

    ${newpass}    Set Variable    ${um}[3]

    ${role}    Set Variable    ${um}[4]

    ${role_split}    Split String From Right    ${role}    ;

    ${agencies}    Set Variable    ${um}[5]


    Run Keyword If    "${info}"=="TEST_ONE"             Run Keywords
    ...               Create User In User Management    ${user}                            ${role_split}[0]         
    ...               AND                               Check Dialog Window If No Exist    ${CREATE_USER_DIALOG}
    # ...               AND                               Check If User Message Exists       Create User ${user} Successfully
    ...               AND                               Select Menu                        ${USER_MANAGEMENT}
    ...               AND                               Verify Results From The Table      ${user}@${domain}
    ...               AND                               Check User Information             ${user}                  ${user}@${domain}    ${role_split}[0]

    Run Keyword If    "${info}"=="TEST_TWO"    Run Keywords                    Input Username And Password    ${user}@${domain}     ${oldpass}
    ...               AND                      Input Update Password Fields    ${newpass}                     AND                   Input Username And Password    ${user}@${domain}    ${newpass}
    ...               AND                      Expand Menu Using Hamburger     AND                            Select Menu           ${USER_MANAGEMENT}
    ...               AND                      Check If Header Is Present      ${USER_MGT_HEADER}             ${USER_MANAGEMENT}
    ...               AND                      Get List Of ToolTips
    ...               AND                      Get List Of Edit Buttons        VISIBLE
    ...               AND                      Get List Of Delete Buttons      HIDDEN
    ...               AND                      Select Menu                     ${ROLE_MANAGEMENT}
    ...               AND                      Check If Header Is Present      ${USER_MGT_HEADER}             ${ROLE_MANAGEMENT}
    ...               AND                      Get List Of ToolTips
    ...               AND                      Get List Of Edit Buttons        VISIBLE
    ...               AND                      Get List Of Delete Buttons      HIDDEN
    ...               AND                      Logout Me

    Run Keyword If    "${info}"=="TEST_THREE"    Run Keywords                   Click Edit Button To Update User Profile    ${user}@${domain}                
    ...               ${role_split}[0]           ${role_split}[1]               AND                                         Verify Results From The Table    ${user}@${domain}    AND                            
    ...               Check User Information     ${user}                        ${user}@${domain}                           ${role_split}[1]                 AND                  Expand Menu Using Hamburger    AND    Logout Me
    ...               AND                        Input Username And Password    ${user}@${domain}                           ${newpass}

    Run Keyword If    "${info}"=="TEST_FIVE"                 Run Keywords         Verify Results From The Table    ${user}@${domain}    AND
    ...               Check User Information                 ${user}              ${user}@${domain}                ${role_split}[0]     AND    
    ...               Attempt To Delete User After Create    ${user}@${domain}    ${user}

    Run Keyword If    "${info}"=="TEST_SIX"    Input Username And Password    ${user}@${domain}    ${newpass}

    END



Create User In User Management
    [Documentation]                     CREATE USER: Creating New User In User Management
    [Arguments]                         ${user}                                              ${role}
    ${USERS}                            Create List                                          ${user}                          ${role}    ${PASSWORD_STRING}
    ...                                 ${PASSWORD_STRING}                                   ${ROLE_DICTIONARY['${role}']}
    Click Button Create
    Check Dialog Window Create User     ${CREATE_USER_DIALOG}
    Input Text                          ${NAME_TEXT}                                         ${user}
    Set Focus To Element                ${EMAIL_TEXT}
    Input Text                          ${EMAIL_TEXT}                                        ${user}\@mailinator.com
    Set Focus To Element                ${PASSWORD_TEXT}
    Input Password                      ${PASSWORD_TEXT}                                     ${PASSWORD_STRING}
    Set Focus To Element                ${PASS_CONFIRM_TEXT}
    Input Password                      ${PASS_CONFIRM_TEXT}                                 ${PASSWORD_STRING}
    Click Element                       ${CREATE_USER_DIALOG}
    Click Element                       ${DROPDOWN_LIST}
    Wait Until Page Contains Element    //li[@aria-label='${ROLE_DICTIONARY['${role}']}']
    Scroll Element Into View            //li[@aria-label='${ROLE_DICTIONARY['${role}']}']
    Click Element                       //li[@aria-label='${ROLE_DICTIONARY['${role}']}']

        # Role Field To Check If data is selected and visible
    Sleep                                               1
    Wait Until Page Contains Element                    ${DROPDOWN_ROLE}
    ${text}                                             Get Text                    ${DROPDOWN_ROLE}
    Should Be Equal                                     ${text}                     ${ROLE_DICTIONARY['${role}']}
    Check If Password And Confirm Password Are Match
    Click Yes Button
    Sleep                                               1
    Click Button Reset
    Log To Console                                      Creating User Management

Check If Password And Confirm Password Are Match
        # To Check Password And Confirm Password If Match
    &{PASS_FIELD}                    Create Dictionary                                        pi_eye=${PASSWORD_TEXT}${PASS_PI_EYE} 
    ...                              pi_eye_slash=${PASSWORD_EYESLASH}${PASS_PI_EYE_SLASH}
    &{CONFIRM_FIELD}                 Create Dictionary                                        pi_eye=${PASS_CONFIRM_TEXT}${PASS_PI_EYE}
    ...                              pi_eye_slash=${CONFIRM_EYESLASH}${PASS_PI_EYE_SLASH}
    ${list}                          Create List                                              ${PASS_FIELD}                                ${CONFIRM_FIELD}
    FOR                              ${item}                                                  IN                                           @{list}
    Wait Until Element Is Visible    ${item['pi_eye']}
    Click Element                    ${item['pi_eye']}
    Wait Until Element Is Enabled    ${item['pi_eye_slash']}
    END
    ${pass_text}                     Get Value                                                ${PASSWORD_EYESLASH}                         
    ${confirm_pass_text}             Get Value                                                ${CONFIRM_EYESLASH}
    Should Be Equal                  ${pass_text}                                             ${confirm_pass_text}

Check If User Message Exists
    [Documentation]             COMMON FUNCTION : Check If Create User Exists
    [Arguments]                 ${message_locator}
    FOR                         ${index}                                         IN RANGE        1    10
    ${count}                    Evaluate                                         1 + ${index}
    Log To Console              ${count}                                         Searching...    
    Wait Until Page Contains    ${message_locator}                               10
    END

Input Update Password Fields
    [Documentation]                                     UPDATE PASSWORD
    [Arguments]                                         ${new_pass}
    Wait Until Page Contains Element                    ${UPDATE_PASS_SCREEN}                                           10
    Input Text                                          ${PASSWORD_TEXT}                                                ${new_pass}
    Input Password                                      ${PASS_CONFIRM_TEXT}                                            ${new_pass}
    Check If Password And Confirm Password Are Match
    Click Element                                       ${SUBMIT_BTN}
    Wait Until Page Contains Element                    ${PROMPT_TOAST}\[contains(text(), "${PROMPT_UPDATE_PASS}")] 

Get List Of ToolTips
    ${list}                             Get WebElements              ${TOOLTIP_SYMBOLS}
    Log                                 ${list}
    ${count_element}                    Get Element Count            ${TOOLTIP_SYMBOLS}
    ${count_list}                       Create List                  ${count_element}
    FOR                                 ${item}                      IN RANGE              @{count_list}
    Wait Until Page Contains Element    ${TOOLTIP_SYMBOLS}
    Log                                 ${item} Element is found 
    END
    Set Suite Variable                  ${count_list}

Get List Of Edit Buttons
    [Arguments]       ${exists}
    ${count}          Get Element Count                      ${EDIT_BTN_SYMBOLS}
    Log               Check Edit Buttons Count : ${count}
    ${count_edit}     Create List                            ${count}
    FOR               ${item}                                IN RANGE                                    @{count_list}
    Run Keyword If    "${exists}"=="VISIBLE"                 Wait Until Page Contains Element            ${EDIT_BTN_SYMBOLS}
    Run Keyword If    "${exists}"=="HIDDEN"                  Wait Until Page Does Not Contain Element    ${EDIT_BTN_SYMBOLS}
    END
    Log               ${count_edit}

Get List Of Delete Buttons
    [Documentation]    USER MANAGEMENT
    [Arguments]        ${exists}
    ${count}           Get Element Count                        ${DELETE_BTN_SYMBOLS}
    Log                Check Delete Buttons Count : ${count}
    ${count_delete}    Create List                              ${count}
    FOR                ${item}                                  IN RANGE                                    @{count_list}
    Run Keyword If     "${exists}"=="VISIBLE"                   Wait Until Page Contains Element            ${DELETE_BTN_SYMBOLS}
    Run Keyword If     "${exists}"=="HIDDEN"                    Wait Until Page Does Not Contain Element    ${DELETE_BTN_SYMBOLS}
    END
    Log                ${count_delete}

Capture Page On Screen
    Set Screenshot Directory    ${OUTPUT DIR}${/}${TEST_NAME}
    Capture Page Screenshot     ${ROLE_MANAGEMENT}.png

Verify Results From The Table
    [Documentation]    USER MANAGEMENT : Verify If The Results Exists
    [Arguments]        ${data}
    ${count}           Get Element Count                                 ${EMAIL_COLUMN}
    ${list}            Create List                                       ${count}

    ${text}    Get Text    //span[@class="p-paginator-current"]    #[normalize-space()="Showing 1 to 10 of 11 products"]    
    Log        ${text}

    ${split}    Split String From Right    ${text}    ${SPACE}
    Log         ${split}
    Log         ${split}[0]
    Log         ${split}[1]
    Log         ${split}[2]
    Log         ${split}[3]
    Log         ${split}[4]
    Log         ${split}[5]

    ${set_list}    Set Variable    ${split}[5]

    ${var_1}       Set Variable    ${set_list}
    ${var_2}       Set Variable    1
    ${var_3}       Set Variable    ${var_1} + ${var_2}
    Log            ${var_3}
    ${var_list}    Create List     ${var_3}

    FOR               ${item}                             IN RANGE                                @{var_list}
    Sleep             3
    ${count}          Evaluate                            ${item} + 1
    Log To Console    \n${count}
        # ${element}  Get Element Count    //tbody/tr[${count}]//td[2][normalize-space()="${data}"]
    ${element}        Get Element Count                   //td[2][normalize-space()="${data}"]
    ${boolean}        Convert To Boolean                  ${element}
    Run Keyword If    ${boolean}==${True}                 Run Keywords                            
    ...               Wait Until Page Contains Element    //td[2][normalize-space()="${data}"]
    ...               AND                                 Click Element                           //td[2][normalize-space()="${data}"]/following::td[2]/div[1]/div[1]//*[name()='svg']
    ...               AND                                 Exit For Loop
    Run Keyword If    ${boolean}==${False}                Run Keywords                            Scroll Element Into View                                                                ${BUTTON_PAGE_NEXT}    AND    Click Element    ${BUTTON_PAGE_NEXT}
    END

    Should Be Equal    ${boolean}    ${True}

Check User Information
    [Documentation]                     USER MANAGEMENT : Checking the User Information
    [Arguments]                         ${name}                                            ${email}    ${roles}    ${permission}=${None}    ${agencies}=${None}
    Wait Until Page Contains Element    ${USER_INFO_HEADER}

    &{info_name}          Create Dictionary    label=NAME           text=${name}          
    &{info_email}         Create Dictionary    label=EMAIL          text=${email}         
    &{info_role}          Create Dictionary    label=ROLES          text=${roles}         
    &{info_permission}    Create Dictionary    label=PERMISSIONS    text=${permission}
    &{info_agencies}      Create Dictionary    label=AGENCIES       text=${agencies}

    ${list}    Create List    ${info_name}    ${info_email}    ${info_role}    #${info_permission}    ${info_agencies}

    FOR                                 ${item}                                                                                                                                        IN    @{list}
    Wait Until Page Contains Element    //div[@class="flex flex-col"]/strong[normalize-space()="${item['label']}"]/following-sibling::div/span[normalize-space()="${item['text']}"]
    END
    Sleep                               2
    Click Element                       ${CLOSE_X_SYMBOL}
Click Edit Button To Update User Profile
    [Documentation]                     USER MANAGEMENT
    [Arguments]                         ${data}              ${role}            ${role_new}
    Wait Until Page Contains Element    ${EMAIL_COLUMN}
    ${count}                            Get Element Count    ${EMAIL_COLUMN}
    ${list}                             Create List          ${count}

    FOR               ${item}                             IN RANGE                                                    @{list}
    ${count}          Evaluate                            ${item} + 1
    Log To Console    \n${count}
    ${element}        Get Element Count                   //tbody/tr[${count}]//td[2][normalize-space()="${data}"]
    ${boolean}        Convert To Boolean                  ${element}
    Run Keyword If    ${boolean}==${True}                 Run Keywords
    ...               Wait Until Page Contains Element    //tbody/tr[${count}]//td[2][normalize-space()="${data}"]
        # This is the EDIT button under role in the table
    ...               AND                                 Click Element                                               //tbody/tr[${count}]//td[2][normalize-space()="${data}"]/following::td[2]/div[1]/div[2]//*[name()='svg']
    ...               AND                                 Exit For Loop
    END

    Should Be Equal    ${boolean}    ${True}

    Sleep                               2
    Wait Until Page Contains Element    ${UPDATE_USER_SCREEN}                                            10
    Wait Until Page Contains Element    ${USER_ROLES_UPDATE}\[text()="${ROLE_DICTIONARY['${role}']}"]    10

    # delete role
    Click Element    ${USER_ROLES_UPDATE}\[text()="${ROLE_DICTIONARY['${role}']}"]/following-sibling::span

    #DROPDOWN LIST BUTTON
    Click Element    ${EDIT_ROLE_LIST}

    #Select role from the dropdown list
    Sleep         2
    ${locator}    Create List    ${DROPDOWN_ROLE_LIST}\[normalize-space()="${ROLE_DICTIONARY['${role_new}']}"]

    FOR                                 ${item}          IN    @{locator}
    Wait Until Page Contains Element    ${item}
    Scroll Element Into View            ${item}
    Click Element                       ${item}
    Click Element                       ${UPDATE_BTN}
    END



Verify User Under Administrator Account
    [Documentation]    USER MANAGEMENT 
    # [Arguments]  ${dashboard}  ${properties}  ${role}  ${user}  ${agency}  ${about}

    ${items}                            Create List                                       ${DASHBOARD_MENU}                                  ${PROPERTIES_MENU}                                 ${ROLE_MANAGEMENT}
    ...                                 ${USER_MANAGEMENT}                                ${AGENCY_MANAGEMENT}                               ${ABOUT_MENU}
    FOR                                 ${item}                                           IN                                                 @{items}
    ${get}                              Get Text                                          ${LIST_OF_MENU}\[normalize-space()="${item}"]
    Log To Console                      Item ${get}
         # SIDE BAR MENU
    Click Element                       ${LIST_OF_MENU}\[normalize-space()="${item}"] 
         # LINKABLE MENU SCREEN PAGE
    Wait Until Page Contains Element    ${LINKABLE_MENU}\[normalize-space()="${get}"]
    ${count}                            Get Element Count                                 ${USER_MGT_HEADER}\[normalize-space()="${get}"]
    Run Keyword If                      ${count} > 0                                      Wait Until Page Contains Element                   ${USER_MGT_HEADER}\[normalize-space()="${get}"]
    END

Attempt To Delete User After Create
    [Documentation]    USER MANAGEMENT : Deleting the User Created
    [Arguments]        ${data}                                        ${user}
    Sleep              3
    ${count}           Get Element Count                              ${EMAIL_COLUMN}
    ${list}            Create List                                    ${count}

    FOR               ${item}                             IN RANGE                                                    @{list}
    ${count}          Evaluate                            ${item} + 1
    Log To Console    \n${count}
    ${element}        Get Element Count                   //tbody/tr[${count}]//td[2][normalize-space()="${data}"]
    ${boolean}        Convert To Boolean                  ${element}
    Run Keyword If    ${boolean}==${True}                 Run Keywords
    ...               Wait Until Page Contains Element    //tbody/tr[${count}]//td[2][normalize-space()="${data}"]
    ...               AND                                 Click Element                                               //tbody/tr[${count}]//td[2][normalize-space()="${data}"]/following::td[2]/div[1]/div[3]//*[name()='svg']
    ...               AND                                 Click Delete Button From PopUp Dialog                       ${user}
    ...               AND                                 Exit For Loop
    END

    # FOR VALIDATING IF THE USER IS DELETED SUCCESSFULLY
    FOR               ${item}                                     IN RANGE                                                    @{list}
    ${count}          Evaluate                                    ${item} + 1
    Log To Console    \n${count}
    ${element}        Get Element Count                           //tbody/tr[${count}]//td[2][normalize-space()="${data}"]
    ${boolean}        Convert To Boolean                          ${element}
    Run Keyword If    ${boolean}==${False}                        Run Keywords
    ...               Wait Until Page Does Not Contain Element    //tbody/tr[${count}]//td[2][normalize-space()="${data}"]
    ...               AND                                         Exit For Loop
    END

    Should Be Equal    ${boolean}    ${False}    They Are Equal As ${False}


