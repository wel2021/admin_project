*** Settings ***
Documentation    This robot file is related to email login functionality
...              including error messages.

Library     SeleniumLibrary
Resource    variables.robot

*** Keywords ***
Verify Error Message From Blank Username And Password
    Wait Until Element Is Visible    ${SUBMIT_BTN}    
    Click Element                    ${SUBMIT_BTN}
    ${list}                          Create List      ${EMAIL_REQUIRED}    ${PASSWORD_REQUIRED}

    FOR                              ${item}                                  IN    @{list}
    Wait Until Element Is Visible    ${EMAIL_ERROR_MSG}\[text()="${item}"]
    END


Input Username And Password
    [Arguments]                         ${user}              ${password_user}
    Wait Until Element Is Visible       ${USER_EMAIL}
    Input Text                          ${USER_EMAIL}        ${user}                                                    #${ADMIN_CRED}
    Input Password                      ${USER_PASSWORD}     ${password_user}                                           #${PASSWORD}
    Wait Until Page Contains Element    ${SUBMIT_BTN}        60
    Click Element                       ${SUBMIT_BTN}
    Sleep                               1
    ${element}                          Get Element Count    ${PROMPT_TOAST}\[normalize-space()="${PROMPT_SUCCESS}"]
    Run Keyword If                      ${element} > 0       Wait Until Page Contains Element                           ${PROMPT_TOAST}\[normalize-space()="${PROMPT_SUCCESS}"]    10

Logout Me
    Sleep                               2
    Wait Until Element Is Visible       ${CURSOR_POINTER}
    Click Element                       ${CURSOR_POINTER}
    Wait Until Element Is Visible       ${LOGOUT_OPTION}
    Click Element                       ${LOGOUT_OPTION}
    Wait Until Element Is Visible       ${USER_EMAIL}
    Wait Until Page Contains Element    ${PROMPT_TOAST}\[normalize-space()="${PROMPT_LOGOUT}"]


Toast Prompt Summary
    [Arguments]                    ${message}
    Wait Until Element Contains    ${PROMPT_TOAST}    ${message}    60
