*** Variables ***
### CREDENTIALS ####
${URL}               https://admin-dev.zrch.dev
${ADMIN_CRED}        ra@zrch.com                   #admin@mailinator.com
${NON_ADMIN_CRED}    agent@mailinator.com
${PASSWORD}          P@ssw0rd456                   #1234567890

### CSV FILE LOCATIONS ###
${USER_MANAGEMENT_CSV}      csv${/}user_management.csv
${AGENCY_MANAGEMENT_CSV}    csv${/}agency_management.csv

### COMMON FUNCTIONS ###
${TITLE}                       Login - Zrch Admin Dev
${CREATE_BTN}                  //span[@class="p-button-label"][text()="Create"]
${CONFIRM_NO}                  //span[@class="p-button-label"][text()="No"]
${CONFIRM_YES}                 //button[@class='p-button p-component p-button']//span[@class='p-button-label'][normalize-space()='Create' or normalize-space()='Yes']
${PROMPT_TOAST}                //span[@class="p-toast-summary"]
${PROMPT_FAIL}                 Login Fail :(
${PROMPT_SUCCESS}              Login Successfully
${PROMPT_LOGOUT}               Logout Successfully
${PROMPT_EMAIL_TAKEN}          The email has already been taken.\
${PROMPT_CREATE_USER}          Create User test Successfully
${PROMPT_DELETE_USER}          Delete User test Successfully
${PROMPT_UPDATE_PASS}          Update Password Successfully
${DROPDOWN_LIST}               //div[@id="role"]//div[@class="p-dropdown-trigger"]                                                                                       
${PASSWORD_TEXT}               //div[@class="p-field mb-6"]//div//input[@id="${PASSWORD_ID}" and @type="${PASSWORD_ID}"]
${PASSWORD_EYESLASH}           //input[@id="password" and @type="text"]
${EYE_BTN}                     //div[@class="p-password p-component p-inputwrapper w-full p-input-icon-right"]
${PASS_PI_EYE}                 /following::i[1][@class="pi pi-eye"]
${PASS_PI_EYE_SLASH}           /following::i[1][@class="pi pi-eye-slash"]
${PASSWORD_ID}                 password
${PASSWORD_CONFIRM_ID}         passwordConfirmation
${PASS_CONFIRM_TEXT}           //div[@class="p-field mb-6"]//div//input[@id="${PASSWORD_CONFIRM_ID}" and @type="${PASSWORD_ID}"]
${CONFIRM_EYESLASH}            //input[@id="${PASSWORD_CONFIRM_ID}" and @type="text"]
${TOOLTIP_SYMBOLS}             //div[@class="flex px-3 justify-center items-center"][1]                                                                                  #/*[local-name()="svg" and @class="cursor-pointer text-primary"]
${TOOLTIP_SYMBOL_2S}           //tbody[@class="p-datatable-tbody"]                                                                                                       #/tr[1]/td[4]/div/child::div[1]
${EDIT_BTN_SYMBOLS}            //div[@class="flex px-3 justify-center items-center"][2]                                                                                  #/*[local-name()="svg" and @class="cursor-pointer text-primary"]
${EDIT_BTN_SYMBOLS_2}          //tbody[@class="p-datatable-tbody"]                                                                                                       #/tr[1]/td[4]/div/child::div[2]
${DELETE_BTN_SYMBOLS}          //div[@class="flex px-3 justify-center items-center"][3]                                                                                  #/*[local-name()="svg" and @class="cursor-pointer text-red-500"]
${DEL_BTN_SYMBOLS_2}           //tbody[@class="p-datatable-tbody"]                                                                                                       #/tr[1]/td[4]/div/child::div[3]
${USER_INFO_HEADER}            //span[@class="p-dialog-title"][normalize-space()="User Info"]
${CLOSE_X_SYMBOL}              //span[@class="p-dialog-header-close-icon pi pi-times"]
${ZRCH_IN_PROGRESS}            html.nprogress-busy
${UPDATE_BTN}                  //span[@class="p-button-label"][normalize-space()="Update"]
${DELETE_BTN}                  //span[@class="p-button-label"][normalize-space()="Delete" or normalize-space()="Yes"]
${LINKABLE_MENU}               //div[@class="inline-flex"]//a                                                                                                            #[normalize-space()="Dashboard"]
${CONFIRM_POP_DELETE}          //span[@class="p-confirm-popup-message"][normalize-space()="Do you want to delete this record?"]
${BUTTON_PAGE_NEXT}            //button[@class="p-paginator-next p-paginator-element p-link"]/child::span[1]
${USER_MGT_HEADER}             //div[@class="p-datatable-header" or @class="p-treetable-header"]//h5                                                                     #[contains(text(), "User Management")]
${RESET_BTN}                   //button[@class="p-button p-component h-11"][normalize-space()="Reset"]
${PAGINATE_DOUBLE_LEFT}        span[class="p-paginator-icon pi pi-angle-double-left"]
${DISABLE_PAGE_DOUBLE_LEFT}    //button[@class="p-paginator-first p-paginator-element p-link p-disabled"]
${UPDATE_USER_LABEL}           span#pv_id_2_header                                                                                                                       # header updated user label when clicking the edit button at the User Management
${UPDATE_USER_STRING}          //span[@id="pv_id_2_header"]                                                                                                              #[normalize-space()="Update User"]


### MENU ###
${DASHBOARD_MENU}       Dashboard
${PROPERTIES_MENU}      Properties
${ROLE_MANAGEMENT}      Role Management
${USER_MANAGEMENT}      User Management
${AGENCY_MANAGEMENT}    Agency Management
${ABOUT_MENU}           About
${MENU_LOCATORS}        //li[contains(@class, "relative overflow-hidden")]//following-sibling::h1                                                                    #[contains(text(), "Role Management")]
${LIST_OF_MENU}         //div[contains(@class, "fixed h-full overflow-hidden")]//ul//li                                                                              #[normalize-space()="Properties"]
${sample}               //div[@class="inline-flex"]//a[normalize-space()="User Management"]
${USER_EMAIL}           id=email
${USER_PASSWORD}        id=password
${SUBMIT_BTN}           //span[@class='p-button-label'][contains(text(), "Submit")]
${LOGO_ZRCH}            div[class='transition-all flex items-center'] > img[alt='Vue logo']
${LOGIN_DISPLAY}        //form[@class="p-fluid mt-8"]//label                                                                                                         #//h3[contains(text(), "Login")]
${EMAIL_REQUIRED}       Email is required
${PASSWORD_REQUIRED}    Password is required
${EMAIL_ERROR_MSG}      //small[@class="p-error"]
${HOME_SCREEN_LOGO}     //span[@class="p-image p-component flex items-center"]
${CURSOR_POINTER}       //span[@class="p-avatar-text"]
${LOGOUT_OPTION}        //span[@class="p-menuitem-text"][normalize-space()="Logout"]
${HAMBURGER_BTN}        //div[contains(@class, "bg-white border-b flex flex-row h-14 p-1")]/child::div[1]
${HAMBURGER_EXPAND}     //div[contains(@class, "bg-white border-b flex flex-row h-14 p-1")]/child::div[1]/*[local-name()="svg" and @class="h-6 w-6 inline-block"]
${HOME_MENU}            //h1[@class="inline-block leading-12 text-sm align-middle pl-2"][contains(text(), "Home")]

### USER MANAGEMENT #####
# ${NAME_STRING}          test
${PASSWORD_STRING}        Qwerty!1
${NEW_PASSWORD_STRING}    Qwerty!2
${CREATE_USER_DIALOG}     //span[@class="p-dialog-title"][text()="Create User"]
${NAME_TEXT}              id=name
${EMAIL_TEXT}             //div[@class="p-field mb-6"]//div//input[@id="email"]
${DROPDOWN_ROLE}          //div[@id="role"]                                                       #div[id='role'] > span[class="p-dropdown-label p-inputtext"]    #id:role
${DROPDOWN_LIST}          //li[@class="p-dropdown-item"]
&{ROLE_DICTIONARY}        super=super_admin                                                       admin=admin                                                     agent=agent    test=test    Vini=Vini
${EMAIL_COLUMN}           //tbody[@class="p-datatable-tbody"]//td[2]
${USER_ROLES_UPDATE}      //ul//li[@class="p-autocomplete-token"]//span                           #[text()="admin"]
${EDIT_ROLE_LIST}         //div[@class='p-field mb-6']//button[@type='button']
${UPDATE_USER_SCREEN}     //span[@class="p-dialog-title"][normalize-space()="Update User"]
${DROPDOWN_ROLE_LIST}     //ul[@class="p-autocomplete-items"]/li[@class="p-autocomplete-item"]

### ROLE MANAGEMENT ###
${ROLE_TEXTFIELD}    //div[@class="flex flex-row h-full"]//span/input[@id="role"]    
#CSS EQUIVALENT      ${ROLE_TEXTFIELD}                                               .flex.flex-row.h-full > span.h-full.p-float-label > #role:nth-of-type(1)
${ROLE_SAVE_BTN}     //span[@class="p-button-label"][normalize-space()="Save"]
${ROLE_COUNT_ROW}    //tbody[@class="p-datatable-tbody"]/tr
${ROLE_EDIT_BTN}     //tbody[@class="p-datatable-tbody"]                             #/tr[${variable}]/td[2]/div/div[${variable}]

### UPDATE PASSWORD ###
${UPDATE_PASS_SCREEN}    //h3[@class="text-2xl text-primary-900"][contains(text(), "Update Password")]


### AGENCY MANAGEMENT ###
${AGENCY_DIALOG}         //span[@class="p-dialog-title"][text()="Create Agency"]
${AGENCY_ID}             id=externalId
${AGENCY_NAME}           id=name
${AGENCY_PARENT_ID}      //div[@role="button"]/following::span[@class="p-dropdown-trigger-icon pi pi-chevron-down"]
${AGENCY_CREATE_BTN}     //button[@class="p-button p-component p-button"]/child::span[1][normalize-space()="Create"]
${AGENCY_NAME_COLUMN}    //tbody[@class="p-treetable-tbody"]//tr
${SELECT_HIGHER_NAME}    //div/ul[@class="p-dropdown-items"]/li                                                         #[normalize-space()="Main"]
${VERIFY_HIGHER_NAME}    //div[@id="parentId"]                                                                          #/div/following::span          #[normalize-space()="Main"]
${AGENCY_DELETE_BTN}     /following::td[1]//*[local-name()="svg" and @class="text-red-500"]                             
                       #Example : //td[2][normalize-space()="Agency2"]/following::td//*[local-name()="svg" and @class="text-red-500"]