*** Keywords ***
List Of Agency Accounts
    [Documentation]    CSV FILE for listing all agencies
    [Arguments]        ${info}                              ${csv}
    ${contents}        Get File                             ${csv}
    @{read}            Create List                          ${contents}
    @{lines}           Split To Lines                       @{read}        1

    FOR      ${line}         IN         @{lines}
    ${am}    Split String    ${line}    ,

    ${agency_name}    Set Variable    ${am}[0]

    ${higher_agency}    Set Variable    ${am}[1]

    Run Keyword If    "${info}"=="ONE"     Run Keywords                                Add Agency Account    ${agency_name}           
    ...               AND                  Agency Dialog Screen Create Button
    ...               AND                  Verify Result Agency At The Table Screen    ${agency_name}        ${AGENCY_NAME_COLUMN}
    Run Keyword If    "${info}"=="FIVE"    Delete Agency                               ${agency_name}        ${AGENCY_NAME_COLUMN}
    END



Add Agency Account
    [Documentation]            AGENCY MANAGEMENT : Creating Newly Created Agency Account
    [Arguments]                ${new_agency}                                                
    Click                      ${CREATE_BTN}
    Wait For Elements State    ${AGENCY_DIALOG}                                             visible          10
    ${random_id}               Random Number
    Log                        ${random_id}
    Fill Text                  ${AGENCY_ID}                                                 ${random_id}
    Fill Text                  ${AGENCY_NAME}                                               ${new_agency}
    Set Suite Variable         ${random_id}


Agency Dialog Screen Create Button
    [Documentation]            Button From the Agency Pop Up Screen
    Click                      ${AGENCY_CREATE_BTN}
    Wait For Elements State    ${AGENCY_DIALOG}                        hidden    10    

Add Higher Agency Office
    [Documentation]    Selecting Higher Office of Agency
    [Arguments]        ${higher_office}
    Click              ${AGENCY_PARENT_ID}
    Click              ${SELECT_HIGHER_NAME}\[normalize-space()="${higher_office}"]
    Get Text           ${VERIFY_HIGHER_NAME}                                           ==    ${higher_office}

Verify Result Agency At The Table Screen
    [Documentation]    COMMON FUNCTION
    [Arguments]        ${data}              ${reference_column}
    Sleep              5
    ${count}           Get Element Count    ${reference_column}
    ${list}            Create List          ${count}

    ${text}    Get Text    //span[@class="p-paginator-current"]    #[normalize-space()="Showing 1 to 10 of 11 products"]    
    Log        ${text}

    ${split}    Split String From Right    ${text}    ${SPACE}
    Log         ${split}
    Log         ${split}[0]
    Log         ${split}[1]
    Log         ${split}[2]
    Log         ${split}[3]
    Log         ${split}[4]
    Log         ${split}[5]

    ${set_list}    Set Variable    ${split}[5]

    ${var_1}       Set Variable    ${set_list}
    ${var_2}       Set Variable    1
    ${var_3}       Set Variable    ${var_1} + ${var_2}
    Log            ${var_3}
    ${var_list}    Create List     ${var_3}

    FOR                    ${item}                                 IN RANGE                                 @{var_list}
    Sleep                  5
    ${old}                 Set Browser Timeout                     30
    ${count}               Evaluate                                ${item} + 1
    Log To Console         \n${count}
    Set Browser Timeout    ${old}
    ${element}             Get Element Count                       //td[2][normalize-space()="${data}"] 
    ${boolean}             Convert To Boolean                      ${element}
    Highlight Elements     //td[2][normalize-space()="${data}"]    duration=120ms                           style=solid                              color=red
    Run Keyword If         ${boolean}==${True}                     Run Keywords                             
    ...                    Wait For Elements State                 //td[2][normalize-space()="${data}"]     visible
    ...                    AND                                     Focus                                    //td[2][normalize-space()="${data}"] 
    ...                    AND                                     Exit For Loop                            
    Run Keyword If         ${boolean}==${False}                    Click                                    ${BUTTON_PAGE_NEXT}
    END

    Should Be Equal    ${boolean}    ${True}

Delete Agency
    [Arguments]                   ${data}                                 ${reference_column}
    Sleep                         5
    Showing Pagination Display    ${reference_column}
    ${var_list}                   Create List                             ${var_3}
    FOR                           ${item}                                 IN RANGE                                                     @{var_list}
    Sleep                         5
    ${old}                        Set Browser Timeout                     30
    ${count}                      Evaluate                                ${item} + 1
    Log To Console                \n${data} is deleted successfully
    Set Browser Timeout           ${old}
    ${element}                    Get Element Count                       //td[2][normalize-space()="${data}"]${AGENCY_DELETE_BTN} 
    ${boolean}                    Convert To Boolean                      ${element}
    Highlight Elements            //td[2][normalize-space()="${data}"]    duration=120ms                                               style=solid                                                 color=red
    Run Keyword If                ${boolean}==${True}                     Run Keywords                                                 
    ...                           Wait For Elements State                 //td[2][normalize-space()="${data}"]                         visible
    ...                           AND                                     Focus                                                        //td[2][normalize-space()="${data}"] 
    ...                           AND                                     Click                                                        //td[2][normalize-space()="${data}"]${AGENCY_DELETE_BTN}
    ...                           AND                                     Click Delete Button From PopUp Message                       ${data}
    ...                           AND                                     Exit For Loop                                                
    Run Keyword If                ${boolean}==${False}                    Click                                                        ${BUTTON_PAGE_NEXT}
    END

    Should Be Equal    ${boolean}    ${True}


     # FOR VALIDATING IF THE USER IS DELETED SUCCESSFULLY
    FOR               ${item}                    IN RANGE                                                    @{list_pagination}    #@{list_pagination} is set suite variable in common functions
    ${count}          Evaluate                   ${item} + 1
    Log To Console    \n${count}
    ${element}        Get Element Count          //tbody/tr[${count}]//td[2][normalize-space()="${data}"]
    ${boolean}        Convert To Boolean         ${element}
    Run Keyword If    ${boolean}==${False}       Run Keywords
    ...               Wait For Elements State    //tbody/tr[${count}]//td[2][normalize-space()="${data}"]    hidden                10
    ...               AND                        Exit For Loop
    END

    Should Be Equal    ${boolean}    ${False}    They Are Equal As ${False}


