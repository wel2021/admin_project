*** Settings ***
Library    SeleniumLibrary
Library    RequestsLibrary
Library    String
Library    OperatingSystem



*** Keywords ***
#-------------ROLE MANAGEMENT-----------------------------------------------------
Input Desired Role Name
    [Documentation]  ROLE MANAGEMENT : Create New User With Role
    [Arguments]     ${role_user}
    Wait Until Page Contains Element    ${ROLE_TEXTFIELD}
    Input Text      ${ROLE_TEXTFIELD}  ${role_user}
    Click Element   ${ROLE_SAVE_BTN}

Verify Results In Table Role Management
    [Documentation]  ROLE MANAGEMENT : Verifying the results after creating the table
    [Arguments]     ${data}

    ${count}  Get Element Count  ${ROLE_COUNT_ROW}
    ${list}   Create List  ${count}

    FOR  ${item}  IN RANGE  @{list}
        ${count}  Evaluate  ${item} + 1
        ${element}  Get Element Count   ${ROLE_COUNT_ROW}\[${count}]/td[1][normalize-space()="${data}"]
        ${boolean}  Convert To Boolean   ${element}
        Run Keyword If  ${boolean}==${True}  Run Keywords
        ...   Wait Until Page Contains Element   ${ROLE_COUNT_ROW}\[${count}]/td[1][normalize-space()="${data}"]
        ...   AND  Click Element   ${ROLE_EDIT_BTN}/tr[${count}]/td[2]/div/div[2]
        ...   AND  Exit For Loop
    END

    Should Be Equal   ${boolean}  ${True}
