

*** Keywords ***
List Of Agency Accounts
    [Documentation]    CSV FILE for listing all agencies
    [Arguments]        ${info}                              ${csv}
    ${contents}        Get File                             ${csv}
    @{read}            Create List                          ${contents}
    @{lines}           Split To Lines                       @{read}        1

    FOR      ${line}         IN         @{lines}
    ${am}    Split String    ${line}    ,

    ${agency_name}    Set Variable    ${am}[0]

    ${higher_agency}    Set Variable    ${am}[1]

    Run Keyword If    "${info}"=="ONE"     Run Keywords                                Add Account Agency    ${agency_name}           
    ...               AND                  Agency Dialog Screen Create Button          
    ...               AND                  Verify Result Agency At The Table Screen    ${agency_name}        ${AGENCY_NAME_COLUMN}
    Run Keyword If    "${info}"=="FIVE"    Delete Agency                               ${agency_name}        ${AGENCY_NAME_COLUMN}
    END



Add Account Agency
    [Documentation]                     AGENCY MANAGEMENT : Creating Newly Created Agency Account
    [Arguments]                         ${new_agency}                                                
    Sleep                               3
    Scroll Up The Page                  
    Wait Until Page Contains Element    ${CREATE_BTN}                                                10
    Click Element                       ${CREATE_BTN}
    Wait Until Page Contains Element    ${AGENCY_DIALOG}                                             10
    ${random_id}                        Random Number
    Log                                 ${random_id}
    Input Text                          ${AGENCY_ID}                                                 ${random_id}
    Input Text                          ${AGENCY_NAME}                                               ${new_agency}
    Set Suite Variable                  ${random_id}

Dialog Message Create Successfully

    FOR                         ${item}                       IN RANGE    1    10
    Sleep                       3
        # ${text}  Get Text    ${PROMPT_TOAST}
        # Wait Until Element Is Visible   ${PROMPT_TOAST}\[normalize-space()="${text}"]  10
    Wait Until Page Contains    Create Agency Successfully    10
    END


Agency Dialog Screen Create Button
    [Documentation]                             Button From the Agency Pop Up Screen
    Click Element                               ${AGENCY_CREATE_BTN}
    Wait Until Page Does Not Contain Element    ${AGENCY_DIALOG}                        10    

Add Higher Agency Office
    [Documentation]    Selecting Higher Office of Agency
    [Arguments]        ${higher_office}
    Click Element      ${AGENCY_PARENT_ID}
    Click Element      ${SELECT_HIGHER_NAME}\[normalize-space()="${higher_office}"]
    ${text}            Get Text                                                        ${VERIFY_HIGHER_NAME}
    Should Be Equal    ${text}                                                         ${higher_office}

Verify Result Agency At The Table Screen
    [Documentation]    COMMON FUNCTION
    [Arguments]        ${data}              ${reference_column}
    Sleep              3
    ${count}           Get Element Count    ${reference_column}
    ${list}            Create List          ${count}

    ${text}    Get Text    //span[@class="p-paginator-current"]    #[normalize-space()="Showing 1 to 10 of 11 products"]    
    Log        ${text}

    ${split}    Split String From Right    ${text}    ${SPACE}
    Log         ${split}
    Log         ${split}[0]
    Log         ${split}[1]
    Log         ${split}[2]
    Log         ${split}[3]
    Log         ${split}[4]
    Log         ${split}[5]

    ${set_list}    Set Variable    ${split}[5]

    ${var_1}       Set Variable    ${set_list}
    ${var_2}       Set Variable    1
    ${var_3}       Set Variable    ${var_1} + ${var_2}
    Log            ${var_3}
    ${var_list}    Create List     ${var_3}

    FOR               ${item}                                              IN RANGE                                 @{var_list}
    Sleep             3
    ${count}          Evaluate                                             ${item} + 1
    Log To Console    \n${count} : ${data} agency is added successfully
    ${element}        Get Element Count                                    //td[2][normalize-space()="${data}"] 
    ${boolean}        Convert To Boolean                                   ${element}
    Run Keyword If    ${boolean}==${True}                                  Run Keywords
    ...               Scroll Element Into View                             //td[2][normalize-space()="${data}"]
    ...               AND                                                  Wait Until Page Contains Element         //td[2][normalize-space()="${data}"]     10
    ...               AND                                                  Set Focus To Element                     //td[2][normalize-space()="${data}"] 
    ...               AND                                                  Exit For Loop                            
    Run Keyword If    ${boolean}==${False}                                 Click Element                            ${BUTTON_PAGE_NEXT}
    END

    Should Be Equal    ${boolean}    ${True}

Delete Agency
    [Arguments]                   ${data}                                         ${reference_column}
    Sleep                         5
    Showing Pagination Display    ${reference_column}
    ${var_list}                   Create List                                     ${var_3}
    FOR                           ${item}                                         IN RANGE                                                     @{var_list}
    ${count}                      Evaluate                                        ${item} + 1
    Log To Console                \n${count} > ${data} is deleted successfully
    ${element}                    Get Element Count                               //td[2][normalize-space()="${data}"]${AGENCY_DELETE_BTN} 
    ${boolean}                    Convert To Boolean                              ${element}
    Run Keyword If                ${boolean}==${True}                             Run Keywords
    ...                           Sleep                                           2
    ...                           AND                                             Scroll Up The Page                                           
    ...                           AND                                             Wait Until Page Contains Element                             //td[2][normalize-space()="${data}"]                        10
    ...                           AND                                             Set Focus To Element                                         //td[2][normalize-space()="${data}"] 
    ...                           AND                                             Click Element                                                //td[2][normalize-space()="${data}"]${AGENCY_DELETE_BTN}
    ...                           AND                                             Click Delete Button From PopUp Dialog                        ${data}
    ...                           AND                                             Exit For Loop                                                
    Run Keyword If                ${boolean}==${False}                            Run Keywords                                                 
    ...                           Wait Until Page Contains Element                ${BUTTON_PAGE_NEXT}                                          30 
    ...                           AND                                             Scroll Down The Page                                         AND                                                         Click Element    ${BUTTON_PAGE_NEXT}
    END

    Should Be Equal    ${boolean}    ${True}


     # FOR VALIDATING IF THE USER IS DELETED SUCCESSFULLY
    FOR               ${item}                                     IN RANGE                                                    @{list_pagination}    #@{list_pagination} is set suite variable in common functions
    ${count}          Evaluate                                    ${item} + 1
    Log To Console    \n${count}
    ${element}        Get Element Count                           //tbody/tr[${count}]//td[2][normalize-space()="${data}"]
    ${boolean}        Convert To Boolean                          ${element}
    Run Keyword If    ${boolean}==${False}                        Run Keywords
    ...               Wait Until Page Does Not Contain Element    //tbody/tr[${count}]//td[2][normalize-space()="${data}"]    10
    ...               AND                                         Exit For Loop
    END

    Should Be Equal    ${boolean}    ${False}    They Are Equal As ${False}


