

*** Keywords ***
Get File And Send File Informations
    [Arguments]       ${info}                    ${csv}
    ${contents}       Get File                   ${csv}
    @{read}           Create List                ${contents}
    @{lines}          Split To Lines             @{read}                                   1
    FOR               ${line}                    IN                                        @{lines}
    ${um}             Split String               ${line}                                   ,
    ${user}           Set Variable               ${um}[0]
    ${domain}         Set Variable               ${um}[1]
    ${oldpass}        Set Variable               ${um}[2]
    ${newpass}        Set Variable               ${um}[3]
    ${role}           Set Variable               ${um}[4]
    ${role_split}     Split String From Right    ${role}                                   ;
    ${agencies}       Set Variable               ${um}[5]
    Run Keyword If    "${info}"=="ONE"           Run Keywords                              Add New User                                                   ${user}                                    ${domain}           ${role_split}[0]    ${oldpass}
    ...               AND                        Verify Result User At The Table Screen    ${user}@${domain}                                              ${EMAIL_COLUMN}
    ...               AND                        Check Tooltip Information                 ${user}                                                        ${user}@${domain}                          ${role_split}[0]
    Run Keyword If    "${info}"=="TWO"           Run Keywords                              Input Valid UserName And Password                              ${user}@${domain}                          ${oldpass}
    ...               AND                        Update Password With New Password         ${newpass}
    ...               AND                        Input Valid UserName And Password         ${user}@${domain}                                              ${newpass}
    ...               AND                        Click Hamburger Button
    ...               AND                        Select Menu From SideBar                  ${USER_MANAGEMENT}
    ...               AND                        Check If Header Is Present                ${USER_MGT_HEADER}\[normalize-space()="${USER_MANAGEMENT}"]
    ...               AND                        Count Available Tooltips
    ...               AND                        Count Available Edit Buttons              VISIBLE
    ...               AND                        Count Available Delete Buttons            HIDDEN
    ...               AND                        Wait For Elements State                   ${CURSOR_POINTER}                                              visible                                    10
    ...               AND                        Click                                     ${CURSOR_POINTER}
    ...               AND                        Wait For Elements State                   ${LOGOUT_OPTION}                                               enabled                                    10                  
    ...               AND                        Hover                                     ${LOGOUT_OPTION}
    ...               AND                        Click                                     ${LOGOUT_OPTION}
    ...               AND                        Reload
    ...               AND                        Sleep                                     3                                                              
    Run Keyword If    "${info}"=="FIVE"          Run Keywords                              Check If Header Is Visible                                     ${USER_MANAGEMENT}
    ...               AND                        Verify Result User At The Table Screen    ${user}@${domain}                                              //tbody[@class="p-datatable-tbody"]//tr
    ...               AND                        Check Tooltip Information                 ${user}                                                        ${user}@${domain}                          ${role_split}[0]
    ...               AND                        Delete Created User                       ${user}@${domain}                                              ${user}

    Run Keyword If    "${info}"=="SIX"    Run Keywords                              Add New User                                                   ${user}                                    ${domain}           ${role_split}[0]    ${oldpass}
    ...               AND                 Verify Result User At The Table Screen    ${user}@${domain}                                              ${EMAIL_COLUMN}
    ...               AND                 Check Tooltip Information                 ${user}                                                        ${user}@${domain}                          ${role_split}[0]
    ...               AND                 Wait For Elements State                   ${CURSOR_POINTER}                                              visible                                    10
    ...               AND                 Click                                     ${CURSOR_POINTER}
    ...               AND                 Wait For Elements State                   ${LOGOUT_OPTION}                                               enabled                                    10                  
    ...               AND                 Hover                                     ${LOGOUT_OPTION}
    ...               AND                 Click                                     ${LOGOUT_OPTION}
    ...               AND                 Sleep                                     5
    ...               AND                 Input Valid UserName And Password         ${user}@${domain}                                              ${oldpass}
    ...               AND                 Update Password With New Password         ${newpass}
    ...               AND                 Input Valid UserName And Password         ${user}@${domain}                                              ${newpass}
    ...               AND                 Click Hamburger Button
    ...               AND                 Select Menu From SideBar                  ${USER_MANAGEMENT}
    ...               AND                 Check If Header Is Present                ${USER_MGT_HEADER}\[normalize-space()="${USER_MANAGEMENT}"]
    ...               AND                 Count Available Tooltips
    ...               AND                 Count Available Edit Buttons              VISIBLE
    ...               AND                 Count Available Delete Buttons            HIDDEN
    ...               AND                 Wait For Elements State                   ${CURSOR_POINTER}                                              visible                                    10
    ...               AND                 Click                                     ${CURSOR_POINTER}
    ...               AND                 Wait For Elements State                   ${LOGOUT_OPTION}                                               enabled                                    10                  
    ...               AND                 Hover                                     ${LOGOUT_OPTION}
    ...               AND                 Click                                     ${LOGOUT_OPTION}
    ...               AND                 Input Valid UserName And Password         ${ADMIN_CRED}                                                  ${PASSWORD}
    ...               AND                 Select Menu From SideBar                  ${USER_MANAGEMENT}
    ...               AND                 Check If Header Is Visible                ${USER_MANAGEMENT}
    ...               AND                 Verify Result User At The Table Screen    ${user}@${domain}                                              //tbody[@class="p-datatable-tbody"]//tr
    ...               AND                 Check Tooltip Information                 ${user}                                                        ${user}@${domain}                          ${role_split}[0]
    ...               AND                 Delete Created User                       ${user}@${domain}                                              ${user}
    END

Create User Dialog Window
    [Documentation]            USER MANAGEMENT
    Wait For Elements State    ${CREATE_USER_DIALOG}    enabled

Add New User
    [Arguments]                                     ${user}                                              ${domain}                                                      ${role}                                                        ${oldpass}
    [Documentation]                                 USER MANAGEMENT
    Click Create Button
    Set Browser Timeout                             10 seconds
    ${old}                                          Set Retry Assertions For                             30s
    Set Browser Timeout                             ${old}
    Create User Dialog Window
    Focus                                           //div[@class="p-dialog-content"]
    Type Text                                       ${NAME_TEXT}                                         ${user}                                                        
    Focus                                           ${EMAIL_TEXT}
    Type Text                                       ${EMAIL_TEXT}                                        ${user}@${domain}                                              
    Focus                                           ${PASSWORD_TEXT}
    Type Text                                       ${PASSWORD_TEXT}                                     ${PASSWORD_STRING}                                             
    Focus                                           ${PASS_CONFIRM_TEXT}
    Type Text                                       ${PASS_CONFIRM_TEXT}                                 ${PASSWORD_STRING}                                             
    Run Keywords                                    Focus                                                ${CREATE_USER_DIALOG}
    ...                                             AND                                                  Click                                                          ${CREATE_USER_DIALOG}
    Wait For Elements State                         ${DROPDOWN_LIST}                                     visible                                                        10
    FOR                                             ${item}                                              IN RANGE                                                       1                                                              11
    Click                                           ${DROPDOWN_LIST}
    Sleep                                           1
    Wait For Elements State                         //li[@aria-label='${ROLE_DICTIONARY['${role}']}']    visible                                                        10
    ${element}                                      Get Element Count                                    //li[@aria-label='${ROLE_DICTIONARY['${role}']}']
    Run Keyword If                                  "${element}"=="1"                                    Run Keywords                                                   Wait For Elements State                                        //li[@aria-label='${ROLE_DICTIONARY['${role}']}']              visible                          10
    ...                                             AND                                                  Click                                                          //li[@aria-label='${ROLE_DICTIONARY['${role}']}']
    ...                                             AND                                                  Get Text                                                       ${DROPDOWN_ROLE}                                               ==                                                             ${ROLE_DICTIONARY['${role}']}
    ...                                             AND                                                  Exit For Loop
    END
    # Type Text    ${DROPDOWN_ROLE}    agency
    Check Password And Confirm Password If Match    ${oldpass}
    Click                                           ${CONFIRM_YES}
    Wait For Elements State                         ${CREATE_USER_DIALOG}                                hidden
    Wait For Elements State                         ${PROMPT_TOAST}                                      visible                                                        10
    ${element}                                      Get Element Count                                    ${PROMPT_TOAST}\[text()="Create User ${user} Successfully"]    #${PROMPT_TOAST}\[text()="${user}"]
    Run Keyword If                                  ${element} == 1                                      Run Keywords                                                   Wait For Elements State                                        ${PROMPT_TOAST}\[text()="Create User ${user} Successfully"]    visible                          10
    ...                                             AND                                                  Wait For Elements State                                        ${PROMPT_TOAST}\[text()="Create User ${user} Successfully"]    hidden                                                         10
    ...                                             ELSE                                                 Log To Console                                                 <<<< Failed >>>>                                               #${PROMPT_TOAST}\[text()="${user}"]                            visible                          10    #ELSE IF    ${element} == 0    # ...    Wait For Elements State    ${PROMPT_TOAST}\[text()="${PROMPT_EMAIL_TAKEN}"]    visible    10
    ${string}                                       Convert To String                                    ${element}
    Should Be Equal                                 ${string}                                            1
    Click Reset Button

Check If Header Is Present
    [Arguments]                ${locator_header}
    [Documentation]            USER MANAGEMENT
    Wait For Elements State    ${locator_header}                           visible
    Log To Console             Checking If The Header Is Presenting....

Check Tooltip Information
    [Arguments]                ${name}                                                                                                                                        ${email}             ${roles}              ${permission}=${None}    ${agencies}=${None}
    [Documentation]            USER MANAGEMENT : Tooltip Informations
    Wait For Elements State    ${USER_INFO_HEADER}                                                                                                                            visible              10
    &{info_name}               Create Dictionary                                                                                                                              label=NAME           text=${name}
    &{info_email}              Create Dictionary                                                                                                                              label=EMAIL          text=${email}
    &{info_role}               Create Dictionary                                                                                                                              label=ROLES          text=${roles}
    &{info_permission}         Create Dictionary                                                                                                                              label=PERMISSIONS    text=${permission}
    &{info_agencies}           Create Dictionary                                                                                                                              label=AGENCIES       text=${agencies}
    ${list}                    Create List                                                                                                                                    ${info_name}         ${info_email}         ${info_role}             #${info_permission}    ${info_agencies}
    FOR                        ${item}                                                                                                                                        IN                   @{list}
    Wait For Elements State    //div[@class="flex flex-col"]/strong[normalize-space()="${item['label']}"]/following-sibling::div/span[normalize-space()="${item['text']}"]
        # Wait For Elements State    //div[@class="flex flex-row"]/strong[normalize-space()="${item['label']}"]/following-sibling::div/span[normalize-space()="${item['text']}"]
    END
    Click                      ${CLOSE_X_SYMBOL}
    Log To Console             Checking The Tooltip Information

Check Password And Confirm Password If Match
    [Arguments]                ${oldpass}
    # To Check Password And Confirm Password If Match
    &{PASS_FIELD}              Create Dictionary                     pi_eye=${PASSWORD_TEXT}${PASS_PI_EYE}        pi_eye_slash=${PASSWORD_EYESLASH}${PASS_PI_EYE_SLASH}
    &{CONFIRM_FIELD}           Create Dictionary                     pi_eye=${PASS_CONFIRM_TEXT}${PASS_PI_EYE}    pi_eye_slash=${CONFIRM_EYESLASH}${PASS_PI_EYE_SLASH}
    ${list}                    Create List                           ${PASS_FIELD}                                ${CONFIRM_FIELD}
    FOR                        ${item}                               IN                                           @{list}
    Wait For Elements State    ${item['pi_eye']}                     visible                                      10
    Click                      ${item['pi_eye']}
    Wait For Elements State    ${item['pi_eye_slash']}               enabled
    END
    FOR                        ${item}                               IN                                           @{list}
    ${get}                     Get Text                              input[id='${PASSWORD_ID}']                   ==                                                       ${oldpass}
    Get Text                   input[id="${PASSWORD_CONFIRM_ID}"]    ==                                           ${get}
    Wait For Elements State    ${item['pi_eye_slash']}               visible                                      10
    Click                      ${item['pi_eye_slash']}
    Wait For Elements State    ${item['pi_eye']}
    END
    ${get}                     Get Text                              input[id='${PASSWORD_ID}']                   ==                                                       ${oldpass}
    Get Text                   input[id="${PASSWORD_CONFIRM_ID}"]    ==                                           ${get}

Update Password With New Password
    [Arguments]                ${new_pass}
    [Documentation]            USER MANAGEMENT : UPDATE PASSWORD
    Wait For Elements State    ${UPDATE_PASS_SCREEN}                visible    10
    # FOR  ${item}  IN RANGE  1  100

    #     &{response}  HTTP  ${URL}${/}password_update
    #     ${integer}  Convert To Integer   200
    #     Should Be Equal  ${response.status}  ${integer}
    #     # Run Keyword If  "${response}"=="200"
    #     # ...  Run Keywords  Should Be Equal   ${response.status}  200
    #     # ...  AND  Log To Console  ${item} : Found ${response.status}  AND   Exit For Loop
    #     Log To Console   \n${item} : Searching.....${response.status}
    # END

    Type Text                       ${PASSWORD_TEXT}           ${new_pass}                                                    
    Keyboard Key                    press                      Tab
    Type Secret                     ${PASS_CONFIRM_TEXT}       $new_pass
    Keyboard Key                    press                      Tab
    # Check Password And Confirm Password If Match    ${newpass}
    Wait For Elements State         ${SUBMIT_BTN}              enabled                                                        10
    # ${select}    Record Selector  Submit    #${SUBMIT_BTN}
    Click                           ${SUBMIT_BTN}              
    Run Keyword And Ignore Error    Wait For Elements State    ${PROMPT_TOAST}\[contains(text(), "${PROMPT_UPDATE_PASS}")]    visible    10

Verify Result User At The Table Screen
    [Arguments]           ${data}                    ${reference_column}
    [Documentation]       COMMON FUNCTION
    Sleep                 1
    ${count}              Get Element Count          ${reference_column}
    ${list_pagination}    Create List                ${count}
    ${text}               Get Text                   //span[@class="p-paginator-current"]    #[normalize-space()="Showing 1 to 10 of 11 products"]
    Log                   ${text}
    ${split}              Split String From Right    ${text}                                 ${SPACE}
    Log                   ${split}
    Log                   ${split}[0]
    Log                   ${split}[1]
    Log                   ${split}[2]
    Log                   ${split}[3]
    Log                   ${split}[4]
    Log                   ${split}[5]
    ${set_list}           Set Variable               ${split}[5]
    ${var_1}              Set Variable               ${set_list}
    ${var_2}              Set Variable               1
    ${var_3}              Set Variable               ${var_1} + ${var_2}
    Log                   ${var_3}
    Set Suite Variable    ${var_3}
    Set Suite Variable    ${list_pagination}
    # Showing Pagination Display    ${reference_column}
    FOR                   ${item}                    IN RANGE                                @{list_pagination}
    Sleep                 1
    ${count}              Evaluate                   ${item} + 1
    Log To Console        \n${count}
    ${element}            Get Element Count          //td[2][normalize-space()="${data}"]
    ${boolean}            Convert To Boolean         ${element}
    Run Keyword If        ${boolean}==${True}        Run Keywords                            Wait For Elements State                                                                 //td[2][normalize-space()="${data}"]    visible
    ...                   AND                        Click                                   //td[2][normalize-space()="${data}"]/following::td[2]/div[1]/div[1]//*[name()='svg']
    ...                   AND                        Exit For Loop
    Run Keyword If        ${boolean}==${False}       Click                                   ${BUTTON_PAGE_NEXT}
    END
    Should Be Equal       ${boolean}                 ${True}

Delete Created User
    [Arguments]        ${data}                                        ${user}
    [Documentation]    USER MANAGEMENT : Deleting the User Created
    Sleep              1
    ${count}           Get Element Count                              ${EMAIL_COLUMN}
    ${list}            Create List                                    ${count}
    FOR                ${item}                                        IN RANGE                                                    @{list}
    ${count}           Evaluate                                       ${item} + 1
    Log To Console     \n${count}
    ${element}         Get Element Count                              //tbody/tr[${count}]//td[2][normalize-space()="${data}"]
    ${boolean}         Convert To Boolean                             ${element}
    Run Keyword If     ${boolean}==${True}                            Run Keywords                                                Wait For Elements State                                                                                     //tbody/tr[${count}]//td[2][normalize-space()="${data}"]    visible                        10
    ...                AND                                            Highlight Elements                                          //tbody/tr[${count}]//td[2][normalize-space()="${data}"]                                                    duration=120ms                                              color=red
    ...                AND                                            Click                                                       //tbody/tr[${count}]//td[2][normalize-space()="${data}"]/following::td[2]/div[1]/div[3]//*[name()='svg']
    ...                AND                                            Click Delete Button From PopUp Message                      ${user}
    ...                AND                                            Click Reset Button
    ...                AND                                            Exit For Loop                                               #...                                                                                                        # AND                                                       Click Paginator Double Left
    END
    # FOR VALIDATING IF THE USER IS DELETED SUCCESSFULLY
    FOR                ${item}                                        IN RANGE                                                    @{list}
    ${count}           Evaluate                                       ${item} + 1
    Log To Console     \n${count}
    ${element}         Get Element Count                              //tbody/tr[${count}]//td[2][normalize-space()="${data}"]
    ${boolean}         Convert To Boolean                             ${element}
    Run Keyword If     ${boolean}==${False}                           Run Keywords                                                Wait For Elements State                                                                                     //tbody/tr[${count}]//td[2][normalize-space()="${data}"]    hidden                         10
    ...                AND                                            Exit For Loop
    END
    Should Be Equal    ${boolean}                                     ${False}                                                    They Are Equal As ${False}


Edit Created User
    [Arguments]        ${data}                                       ${user}
    [Documentation]    USER MANAGEMENT : Editing the User Created
    Sleep              1
    ${count}           Get Element Count                             ${EMAIL_COLUMN}
    ${list}            Create List                                   ${count}
    FOR                ${item}                                       IN RANGE                                                    @{list}
    ${count}           Evaluate                                      ${item} + 1
    Log To Console     \n${count}
    ${element}         Get Element Count                             //tbody/tr[${count}]//td[2][normalize-space()="${data}"]
    ${boolean}         Convert To Boolean                            ${element}
    Run Keyword If     ${boolean}==${True}                           Run Keywords                                                Wait For Elements State                                                                                     //tbody/tr[${count}]//td[2][normalize-space()="${data}"]    visible                        10
    ...                AND                                           Highlight Elements                                          //tbody/tr[${count}]//td[2][normalize-space()="${data}"]                                                    duration=120ms                                              color=red
    ...                AND                                           Click                                                       //tbody/tr[${count}]//td[2][normalize-space()="${data}"]/following::td[2]/div[1]/div[2]//*[name()='svg']
    ...                AND                                           Exit For Loop                                               #...                                                                                                        # AND                                                       Click Paginator Double Left
    END

    ${text}                    Get Text                                               ${UPDATE_USER_LABEL}
    Wait For Elements State    ${UPDATE_USER_STRING}$\[normalize-space()="{text}"]    visible                 10





