*** Settings *** 
Documentation    This library is intended for generating/capturing video format and/or taking screenshots
...              after the test execution.
Library     ScreenCapLibrary




*** Keywords ***
Start Capturing Video
    [Documentation]   Start Video Recording
    [Arguments]  ${monitor}=${None}
    Start Video Recording      alias=${SUITE NAME}  name=${SUITE NAME}   monitor=${monitor}

Stop Capturing Video
    [Documentation]   Stop Video Recording
    Stop Video Recording        alias=${SUITE NAME}

Start Capturing GIF
    [Documentation]   Start Gif Recording
    [Arguments]  ${monitor}=${None}
    Start Gif Recording    name=${SUITE NAME}  monitor=${monitor}  size_percentage=1

Stop Capturing GIF
    [Documentation]  Stop Gif Recording
    Stop Gif Recording