*** Settings ***
Library    RPA.Outlook.Application
Test Setup   Open Application
Test Teardown  Quit Application

*** Variables ***
${RECIPIENT}    ra@zrch.com
${BODY}         This is the message body
${SUBJECT}      This is the subject


*** Test Cases ***
Sending Message
    Send Message    recipients=${RECIPIENT}
    ...             subject=${SUBJECT}
    ...             body=${BODY}

Waiting Message
    Wait For Message   SUBJECT:${SUBJECT}
    ...                timeout=150
    ...                interval=10